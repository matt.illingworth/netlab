#!/bin/sh
# Install script downloaded from EVE-NG

wget -O - http://www.eve-ng.net/repo/eczema@ecze.com.gpg.key | sudo apt-key add -
apt-get update
apt-get -y install software-properties-common

echo "deb [arch=amd64] http://www.eve-ng.net/repo xenial main" > /etc/apt/sources.list.d/eve-ng.list
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng
/etc/init.d/mysql restart
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng
rm -fr /var/lib/docker/aufs
DEBIAN_FRONTEND=noninteractive apt-get -y install eve-ng

echo "Eve-ng installed" > /root/eve-installed.txt

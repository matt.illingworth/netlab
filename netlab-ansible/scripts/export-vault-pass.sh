#!/bin/bash

if [ -z $ANSIBLE_VAULT_PASSWORD ]
then
  read -s -p "Vault Password: " vault_password
  export ANSIBLE_VAULT_PASSWORD=$vault_password
fi

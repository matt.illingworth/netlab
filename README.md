# netlab

A virtual networking lab built using terraform and ansible, powered by openstack and eve-ng

You need to have have a version of python3 installed along with tox.

- From the toplevel netlab directory run 'make install' this will create the venv and setup terraform + ansible.
- source you openstack creds 'source sausage_creds.sh'
- source the venv 'source venv/bin/activate'
- To build the lab change into the netlab-terraform directory and run 'make plan' / 'make apply'.

Eve-ng requries passwords to be set for users, these hashes will need to be generated and then encrypted
useing ansible vault.  The encrypted vault secrets will then need to be stored in roles/eve/vars/main.yml

- To generate a hash 'openssl passwd -6 -salt xyz "your_root_password"'
- To encrypt the hash 'ansible-vault encrypt_string --ask-vault-password 'your_root_hash' --name 'root_hash'
- Copy the encrypted root_hash into roles/eve/vars/main.yml
- Repeat the process for the ubuntu_hash
- Run ansible to configure hosts; cd netlab-ansible; 'make plan' / 'make apply'.
- From netlab-ansible directory 'ssh -F ssh.cfg root@netlab-jump.cloud.mattillingworth.com'
- Follow the final eve setup bits, it will then reboot.
- from the ansible directory 'ssh -F ssh.cfg netlab-jump.cloud.mattillingworth.com' This adds a dynamic socks forwarder.
- From a browser setup with socks forwarding browse to http://192.168.40.200

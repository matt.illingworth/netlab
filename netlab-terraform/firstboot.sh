#!/bin/sh

# set defaults
tmp="/root/"

# check for root privilege
if [ "$(id -u)" != "0" ]; then
   echo " this script must be run as root" 1>&2
   echo
   exit 1
fi


# determine ubuntu version
ubuntu_version=$(lsb_release -cs)

#puppet_deb="puppet-release-"$ubuntu_version".deb"

# print status message
echo " preparing your server; this may take a few minutes ..."


# set fqdn
fqdn="$default_hostname.$default_domain"

# update hostname
#echo "$default_hostname" > /etc/hostname
#sed -i "s@ubuntu.ubuntu@$fqdn@g" /etc/hosts
#sed -i "s@ubuntu@$fqdn $default_hostname@g" /etc/hosts
#hostname "$default_hostname"
export DEBIAN_FRONTEND=noninteractive
# update repos
apt-get -y update
apt-get -y install net-tools python-apt htop

echo "firstboot script ran" > /tmp/firstboot.log

# Delete me
#rm -f $0
mv /etc/rc.local /root/rc.local.bak

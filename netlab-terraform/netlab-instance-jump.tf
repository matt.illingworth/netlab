resource "openstack_compute_instance_v2" "instance_netlab_jump" {
  name            = "netlab-jump.cloud.mattillingworth.com"
  image_name      = "Ubuntu 20.04 (20200714)"
  flavor_name     = "frankfurter"
  user_data       = "${file("firstboot.sh")}"
  key_pair        = "${openstack_compute_keypair_v2.matt-cloud-key.name}"
  security_groups = ["netlab_secgroup_1"]

  network {
    access_network = true
    name = "netlab_oob_network"
    port = "${openstack_networking_port_v2.port_netlab_jump_oob.id}"
  }
  network {
    name = "netlab_mgmt_network"
    port = "${openstack_networking_port_v2.port_netlab_jump_mgmt.id}"
  }
}

resource "openstack_dns_recordset_v2" "rs_instance_netlab_jump" {
  zone_id = "${openstack_dns_zone_v2.zone_cloud_mattillingworth_com.id}"
  name = "netlab-jump.cloud.mattillingworth.com."
  description = "netlab jump record"
  ttl = 600
  type = "A"
  records = ["${openstack_networking_floatingip_v2.fip_netlab_jump.address}"]
}

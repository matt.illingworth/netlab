resource "openstack_networking_network_v2" "network_netlab_oob" {
  name           = "netlab_oob_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_netlab_oob" {
  name       = "netlab_oob_subnet"
  network_id = "${openstack_networking_network_v2.network_netlab_oob.id}"
  cidr       = "192.168.40.0/24"
  dns_nameservers = ["8.8.8.8", "1.1.1.1"]
  ip_version = 4
}

resource "openstack_networking_router_v2" "router_netlab" {
  name                = "netlab_router"
  admin_state_up      = true
  external_network_id = "5617d17e-fdc1-4aa1-a14b-b9b5136c65af"
}

resource "openstack_networking_router_route_v2" "remote_ipsec" {
  depends_on       = ["openstack_networking_router_interface_v2.netlab_router_interface_1"]
  router_id        = "${openstack_networking_router_v2.router_netlab.id}"
  destination_cidr = "192.168.50.0/24"
  next_hop         = "192.168.40.10"
}

resource "openstack_networking_router_route_v2" "remote_wireguard" {
  depends_on       = ["openstack_networking_router_interface_v2.netlab_router_interface_1"]
  router_id        = "${openstack_networking_router_v2.router_netlab.id}"
  destination_cidr = "192.168.60.0/24"
  next_hop         = "192.168.40.10"
}

resource "openstack_networking_router_interface_v2" "netlab_router_interface_1" {
  router_id = "${openstack_networking_router_v2.router_netlab.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_netlab_oob.id}"
}

resource "openstack_networking_floatingip_v2" "fip_netlab_jump" {
  pool = "internet"
}

resource "openstack_compute_floatingip_associate_v2" "fip_netlab_jump" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_netlab_jump.address}"
  instance_id = "${openstack_compute_instance_v2.instance_netlab_jump.id}"
}

resource "openstack_networking_port_v2" "port_netlab_vpn_oob" {
  name                   = "netlab_vpn_oob_port"
  network_id             = "${openstack_networking_network_v2.network_netlab_oob.id}"
  admin_state_up         = "true"
  port_security_enabled  = "false"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_netlab_oob.id}"
    ip_address = "192.168.40.10"
  }
}

resource "openstack_networking_port_v2" "port_netlab_jump_oob" {
  name               = "netlab_jump_oob_internal_port"
  network_id         = "${openstack_networking_network_v2.network_netlab_oob.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_networking_secgroup_v2.netlab_secgroup_1.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_netlab_oob.id}"
    ip_address = "192.168.40.100"
  }
}

resource "openstack_networking_port_v2" "port_netlab_eve_oob" {
  name               = "netlab_eve_oob_internal_port"
  network_id         = "${openstack_networking_network_v2.network_netlab_oob.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_networking_secgroup_v2.netlab_secgroup_1.id}", "${openstack_networking_secgroup_v2.netlab_secgroup_eve.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_netlab_oob.id}"
    ip_address = "192.168.40.200"
  }
}

# Mgmt network below used to connect mgmt interfaces on virtual network devices running
# within EVE-NG to the jumpbox on a separate interface.  DHCP is turned off on this network
# and configured to run on Jump so that it can offer things like Junos ZTP

resource "openstack_networking_network_v2" "network_netlab_mgmt" {
  name           = "netlab_mgmt_network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_netlab_mgmt" {
  name       = "netlab_mgmt_subnet"
  network_id = "${openstack_networking_network_v2.network_netlab_mgmt.id}"
  cidr       = "192.168.99.0/24"
  enable_dhcp = "false"
  no_gateway = "true"
  ip_version = 4
}

resource "openstack_networking_port_v2" "port_netlab_jump_mgmt" {
  name               = "netlab_jump_mgmt_internal_port"
  network_id         = "${openstack_networking_network_v2.network_netlab_mgmt.id}"
  admin_state_up     = "true"
  port_security_enabled  = "false"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_netlab_mgmt.id}"
    ip_address = "192.168.99.100"
  }
}

resource "openstack_networking_port_v2" "port_netlab_eve_mgmt" {
  name               = "netlab_eve_mgmt_internal_port"
  network_id         = "${openstack_networking_network_v2.network_netlab_mgmt.id}"
  admin_state_up     = "true"
  port_security_enabled  = "false"

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_netlab_mgmt.id}"
    ip_address = "192.168.99.200"
  }
}

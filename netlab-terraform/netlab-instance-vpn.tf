resource "openstack_compute_instance_v2" "netlab-vpn" {
  name            = "netlab-vpn.cloud.mattillingworth.com"
  image_name      = "Ubuntu 20.04 (20200714)"
  config_drive    = true
  flavor_name     = "frankfurter"
  user_data       = "${file("firstboot.sh")}"
  key_pair        = "${openstack_compute_keypair_v2.matt-cloud-key.name}"
  security_groups = ["default","netlab_secgroup_1"]

  network {
    name = "direct_internet"
  }
  network {
    name = "netlab_oob_network"
    port = "${openstack_networking_port_v2.port_netlab_vpn_oob.id}"
  }
}

resource "openstack_dns_recordset_v2" "rs_instance_vpn" {
  zone_id = "${openstack_dns_zone_v2.zone_cloud_mattillingworth_com.id}"
  name = "netlab-vpn.cloud.mattillingworth.com."
  description = "netlab vpn record"
  ttl = 600
  type = "A"
  records = ["${openstack_compute_instance_v2.netlab-vpn.access_ip_v4}"]
}

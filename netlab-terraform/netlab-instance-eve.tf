resource "openstack_compute_instance_v2" "instance_netlab_eve" {
  name            = "netlab-eve.cloud.mattillingworth.com"
  image_name      = "Ubuntu 16.04 (20201119)"
  flavor_name     = "cumberland"
  user_data       = "${file("firstboot.sh")}"
  key_pair        = "${openstack_compute_keypair_v2.matt-cloud-key.name}"
  #security_groups = ["netlab_secgroup_1", "netlab_secgroup_eve"]

  network {
    name = "netlab_oob_network"
    port = "${openstack_networking_port_v2.port_netlab_eve_oob.id}"
    access_network = true
  }
  network {
    name = "netlab_mgmt_network"
    port = "${openstack_networking_port_v2.port_netlab_eve_mgmt.id}"
  }
}

#resource "openstack_dns_recordset_v2" "rs_instance_netlab_eve" {
#  zone_id = "${openstack_dns_zone_v2.zone_cloud_mattillingworth_com.id}"
#  name = "netlab-eve.cloud.mattillingworth.com."
#  description = "netlab eve record"
#  ttl = 600
#  type = "A"
#  records = ["${openstack_networking_floatingip_v2.fip_netlab_eve.address}"]
#}
